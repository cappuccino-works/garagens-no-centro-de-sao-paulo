Para usar este notebook, você precisa da base de dados do IPTU de São Paulo (disponível em http://dados.prefeitura.sp.gov.br/dataset/base-de-dados-do-imposto-predial-e-territorial-urbano-iptu).

A base vem num CSV, com todos os campos em formato texto (o que, diga-se de passagem, eu, como consumidor de dados, ADOREI).
Importei num SQLite e coloquei alguns campos para facilitar a vida, por exemplo, convertendo para números.

Para facilitar a vida de todo mundo que quiser usar os dados, deixei a base do IPTU em formato SQLite com as adições [num repositório separado](https://gitlab.com/cappuccino-works/base-do-iptu).

Quanto aos mapas, você vai precisar do [QGIS](http://qgis.org/) e do [Mapa Digital da Cidade de São Paulo](http://dados.prefeitura.sp.gov.br/dataset/mapa-digital-da-cidade-mdc-sao-paulo), mais especificamente da camada de distritos.
